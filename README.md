
# Einstein Toolit Jenkins Container

To start a container suitable for acting as a Jenkins slave, on your
Docker host, run

    docker run --restart=always -d --name etslave -p 2023:22 ianhinder/et-jenkins-slave:ubuntu-12.04

You can name it whatever you like instead of etslave, and adapt the
ssh source port 2023 to whatever port you want to listen on for ssh
connections from the Jenkins master.

Contact the ET Jenkins administrator to have your slave added to the
Jenkins system.  You will need to provide the host and port for ssh
connections.

You can choose another operating system by specifying a different tag, i.e.

    ianhinder/et-jenkins-slave:<tag>

The available tags are listed at https://hub.docker.com/r/ianhinder/et-jenkins-slave/tags/.

You can add your own ssh key to the container using

    docker exec -i etslave sh -c "cat >>/home/jenkins/.ssh/authorized_keys" < ~/.ssh/id_rsa.pub

and then log in as usual via ssh:

    ssh -p 2023 jenkins@localhost

# Cleaning up

You can stop the container with

    docker stop etslave

and restart it with

    docker start etslave

You can remove the container, including any changes you have made to
its filesystem, with

    docker rm etslave
